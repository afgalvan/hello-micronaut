package hello.micronaut;

import io.micronaut.runtime.EmbeddedApplication;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import jakarta.inject.Inject;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@MicronautTest
class HelloMicronautTest {
    @Inject
    EmbeddedApplication application;

    @Inject
    HelloClient helloClient;

    @Inject
    EmbeddedServer server;

    @Inject
    @Client("/")
    HttpClient client;

    @Test
    void testItWorks() {
        assertTrue(application.isRunning());
    }

    @Test
    void testHelloWorldResponse() {
        String response = client.toBlocking()
                .retrieve(HttpRequest.GET("/hello"));
        assertEquals("<h1>Hello, World!</h1>", response);
    }

    @Test
    void testHelloClient() {
        assertEquals("<h1>Hello, World!</h1>", Mono.from(helloClient.hello()).block());
    }
}