package hello.micronaut.controllers;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/hello")
public class HelloController {
    @Get(produces = MediaType.TEXT_HTML)
    public String index() {
        return "<h1>Hello, World!</h1>";
    }
}
