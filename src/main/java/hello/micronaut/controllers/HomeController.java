package hello.micronaut.controllers;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/")
public class HomeController {
    @Get(produces = MediaType.TEXT_HTML)
    public String getHome() {
        return "<h1>Containerized Micronaut app with nginx!</h1>" +
                "<a href=\"/hello\">Hello world</a> </br>" +
                "<a href=\"/swagger-ui\">Swagger</a>";
    }
}
