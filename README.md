# Micronaut test API

A simple web for trying micronaut

## Requirements

- [Docker](https://docs.docker.com/get-docker/)
- [docker compose](https://docs.docker.com/compose/install/)

## Local run

```shell
docker-compose up --build
```

With hot reload
```shell
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build
```
