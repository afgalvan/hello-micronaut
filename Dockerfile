FROM gradle:7.4-jdk17-alpine AS base

WORKDIR /api

COPY *gradle* ./

RUN mkdir gradle

RUN mv /api/wrapper /api/gradle

RUN ./gradlew dependencies

FROM base as run

COPY . .

EXPOSE 8000

ENTRYPOINT [ "./gradlew" ]
